from app.App import getApplication
from core.logger import Logger
from core.master import Master
from core.config.config import ConfigMainModel, configExample
from pathlib import Path

import uvicorn

if __name__ == "__main__":
    configPath = Path("config.json")
    config = configExample
    
    if configPath.exists():
        config = ConfigMainModel.fromFile(configPath)
        config.save(configPath)
        logger = Logger(name=config.general.logging.name,
                        level=config.general.logging.level,
                        path=config.general.logging.path)
        logger.info(f"Load config from file: {configPath.absolute()}")
        
    
    else:
        logger = Logger(name=config.general.logging.name,
                        level=config.general.logging.level,
                        path=config.general.logging.path)
        logger.warning(f'Config file "{configPath}" not found, try use default.')
        config.save(configPath)
        
    logger.info(f"Use config: {config.model_dump_json(indent=4)}")    
    
    with Master(config=config, logger=logger) as master:
        app = getApplication(config=config)
        
        uvicorn.run(app, host=str(config.webServer.host), port=config.webServer.port)

    