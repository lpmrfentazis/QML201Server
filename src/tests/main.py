import unittest

if __name__ == "__main__":
    testLoader = unittest.TestLoader()
    testSuite = testLoader.discover(start_dir='.', top_level_dir='.', pattern='test*.py', )
    unittest.TextTestRunner().run(testSuite)
