# https://stackoverflow.com/questions/51896862/how-to-create-singleton-class-with-arguments-in-python
from __future__ import annotations
import threading
from typing import Any



class ThreadSafeSingleton(type):
    __instances: dict[type, ThreadSafeSingleton] = {}
    __singleton_locks: dict[Any, threading.Lock] = {}
    
    def __call__(cls, *args: Any, **kwargs: dict[Any, Any]) -> ThreadSafeSingleton:
        # double-checked locking pattern (https://en.wikipedia.org/wiki/Double-checked_locking)
        
        if cls not in cls.__instances:
            if cls not in cls.__singleton_locks:
                cls.__singleton_locks[cls] = threading.Lock()
                
            with cls.__singleton_locks[cls]:
                if cls not in cls.__instances:
                    cls.__instances[cls] = super(ThreadSafeSingleton, cls).__call__(*args, **kwargs)
                    
        return cls.__instances[cls]
