"""
Main configuration model for the application.

This module defines the main configuration model for the application, which includes
general settings, web server settings, database settings, and logger settings.

The `ConfigMainModel` class inherits from `ConfigBaseModel` and contains instances of
the following models:

- `ConfigGeneralModel`
- `ConfigWebServerModel`
- `ConfigDatabaseModel`
- `ConfigLoggerModel`

The class provides methods to save the configuration to a file and load it from a file.

Attributes:
    general (ConfigGeneralModel): General configuration settings.
    webServer (ConfigWebServerModel): Web server configuration settings.
    database (ConfigDatabaseModel): Database configuration settings.
    logger (ConfigLoggerModel): Logger configuration settings.

Examples:
    >>> config = ConfigMainModel()
    >>> config.save(Path("config.json"))
    >>> loaded_config = ConfigMainModel.fromFile(Path("config.json"))
"""

from __future__ import annotations
from .models import (ConfigBaseModel, 
                     ConfigGeneralModel,
                     ConfigDatabaseModel,
                     ConfigLoggerModel,
                     ConfigWebServerModel)
from pathlib import Path
from typing import Any
import json

class ConfigMainModel(ConfigBaseModel):
    """
    Main configuration model for the application.

    Attributes:
        general (ConfigGeneralModel): General configuration settings.
        webServer (ConfigWebServerModel): Web server configuration settings.
        database (ConfigDatabaseModel): Database configuration settings.
        logger (ConfigLoggerModel): Logger configuration settings.
    """
    general: ConfigGeneralModel = ConfigGeneralModel()
    webServer: ConfigWebServerModel = ConfigWebServerModel()
    database: ConfigDatabaseModel = ConfigDatabaseModel()
    logger: ConfigLoggerModel = ConfigLoggerModel()
    
    def save(self, path: Path) -> None:
        """
        Save the configuration to a file.

        Args:
            path (Path): Path to the file where the configuration will be saved.
        """
        with open(path, "w") as f:
            f.write(self.model_dump_json(indent=4))
    
    @classmethod
    def fromFile(cls, path: Path) -> ConfigMainModel:
        """
        Load the configuration from a file.

        Args:
            path (Path): Path to the file containing the configuration.

        Returns:
            ConfigMainModel: Loaded configuration model.
        """
        default  = cls()
        with open (path, "r") as f:
            data: dict[str, Any] = json.load(f)
            default.model_dump(mode="json").update(data)
            
            return cls(**data)
        
configExample = ConfigMainModel()