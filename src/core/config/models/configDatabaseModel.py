from .configBaseModel import ConfigBaseModel
from pydantic import Field



class ConfigDatabaseModel(ConfigBaseModel):
    dsn: str = Field(pattern=r'sqlite:\/\/\/([^\?]+)(?:\?(.*))?$', 
                     default="sqlite+aiosqlite:///db.sqlite")