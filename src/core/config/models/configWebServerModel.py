from .configBaseModel import ConfigBaseModel
from pydantic import IPvAnyAddress
from typing import Annotated
from annotated_types import Ge, Le

    
class ConfigWebServerModel(ConfigBaseModel):
    host: IPvAnyAddress = IPvAnyAddress("0.0.0.0")
    port: Annotated[int, Ge(80), Le(65534)] = 80
    allowCORS: bool = True