"""
Base configuration model for the application.

This module defines the base configuration model for the application, which serves as
a foundation for other configuration models. It inherits from `pydantic.BaseModel`
and uses `pydantic.ConfigDict` to configure the model behavior.

The `ConfigBaseModel` class provides the following configuration settings:

- `from_attributes`: Enables loading values from class attributes.
- `validate_default`: Enables validation of default values.

Attributes:
    model_config (ConfigDict): Configuration settings for the model.

Examples:
    >>> class MyModel(ConfigBaseModel):
    ...     field1: str
    ...     field2: int
    ...
    >>> model = MyModel(field1="value1", field2=42)
    >>> print(model)
    MyModel(field1='value1', field2=42)
"""

from pydantic import BaseModel, ConfigDict

class ConfigBaseModel(BaseModel): # type: ignore
    """
    Base configuration model for the application.

    Attributes:
        model_config (ConfigDict): Configuration settings for the model.
    """
    model_config = ConfigDict(
        from_attributes = True,
        validate_default = True
    )