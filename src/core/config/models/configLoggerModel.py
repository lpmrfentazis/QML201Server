from .configBaseModel import ConfigBaseModel
from typing import Annotated, Optional
from annotated_types import Ge, Le
from pydantic import Field
from typing import Literal, Union

from enum import Enum


class ConfigConnectionType(str, Enum):
    serial = "serial"

class ConfigPlatformType(str, Enum):
    QML201 = "QML201"

class ConfigLoggerBaseConnectionModel(ConfigBaseModel):
    type: ConfigConnectionType
    
class ConfigLoggerSerialConnectionModel(ConfigLoggerBaseConnectionModel):
    type: Literal[ConfigConnectionType.serial] = ConfigConnectionType.serial
    port: str = "auto"
    baudrate: int = 9600
    timeout: float = 5
    description: Optional[str] = "MOXA USB Serial Port"
    deviceVID: Optional[int] = 0
       
class ConfigLoggerModel(ConfigBaseModel):
    platform: ConfigPlatformType = ConfigPlatformType.QML201
    connection: Union[ConfigLoggerSerialConnectionModel] = Field(discriminator="type", 
                                                           default=ConfigLoggerSerialConnectionModel())
    getInfoTimeout: Annotated[float, Ge(0.1), Le(5)] = 0.5
