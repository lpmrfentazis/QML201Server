from __future__ import annotations
from enum import IntEnum

from ..configBaseModel import ConfigBaseModel
from pydantic import field_validator, field_serializer

from logging import (DEBUG, INFO, WARNING, ERROR, CRITICAL)
from verboselogs import (VERBOSE, SPAM)
from typing import Literal
from pathlib import Path

from typing import Annotated, Union
from annotated_types import MinLen, Ge, Le
from typing import Any


class ConfigLoggingLevel(IntEnum):
    CRITICAL = CRITICAL
    ERROR = ERROR  
    WARNING = WARNING
    INFO = INFO
    VERBOSE = VERBOSE
    DEBUG = DEBUG
    SPAM = SPAM
    

class ConfigGeneralLoggingModel(ConfigBaseModel):
    name: Annotated[str, MinLen(1)] = "App"
    level: Literal[ConfigLoggingLevel.SPAM, 
                          ConfigLoggingLevel.DEBUG,
                          ConfigLoggingLevel.VERBOSE,
                          ConfigLoggingLevel.INFO,
                          ConfigLoggingLevel.WARNING,
                          ConfigLoggingLevel.ERROR,
                          ConfigLoggingLevel.CRITICAL] = ConfigLoggingLevel.INFO
    path: Path = Path(".")
    messageBufferSize: Annotated[int, Ge(0), Le(500)] = 200
    
    @field_serializer("level") # type: ignore[misc]
    def loggingLevelSerializer(self, level: ConfigLoggingLevel, _info: Any) -> str:
        match level:
            case ConfigLoggingLevel.SPAM:
                return "spam"
            case ConfigLoggingLevel.DEBUG:
                return "debug"
            case ConfigLoggingLevel.VERBOSE:
                return "verbose"
            case ConfigLoggingLevel.INFO:
                return "info"
            case ConfigLoggingLevel.WARNING:
                return "warning"
            case ConfigLoggingLevel.ERROR:
                return "error"
            case ConfigLoggingLevel.CRITICAL:
                return "critical"
            
            case _:
                raise ValueError("Unprocessed ConfigLoggingLevel you probably forgot to add a case for this one")
    
    @field_validator("level", mode="before") # type: ignore
    def loggingLevelValidator(cls, value: Union[int, str]) -> ConfigLoggingLevel:
        if isinstance(value, int):
            try:
                return ConfigLoggingLevel(value)
                 
            except:
                raise ValueError("Must be one of [spam, debul, verbose, info, warning, error, critical]")
        
        elif isinstance(value, str):
            val = value.lower()
            match val:
                case "spam":
                    return ConfigLoggingLevel.SPAM
                case "debug":
                    return ConfigLoggingLevel.DEBUG
                case "verbose":
                    return ConfigLoggingLevel.VERBOSE
                case "info":
                    return ConfigLoggingLevel.INFO
                case "warning":
                    return ConfigLoggingLevel.WARNING
                case "error":
                    return ConfigLoggingLevel.ERROR
                case "critical":
                    return ConfigLoggingLevel.CRITICAL
                case _:
                    raise ValueError("Must be one of [spam, debul, verbose, info, warning, error, critical]")
        else:
            raise ValueError("Must be int or str contained int value")