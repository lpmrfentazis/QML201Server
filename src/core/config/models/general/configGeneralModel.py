from __future__ import annotations

from ..configBaseModel import ConfigBaseModel
from .configGeneralLoggingModel import ConfigGeneralLoggingModel
from pathlib import Path
from pydantic import model_validator



class ConfigGeneralModel(ConfigBaseModel):
    cwd: Path = Path("")
    logging: ConfigGeneralLoggingModel = ConfigGeneralLoggingModel()
    
    @model_validator(mode="after") # type: ignore
    def modelAfterValidation(self) -> ConfigGeneralModel:
        
        self.cwd.mkdir(exist_ok=True, parents=True)
        
        return self
        
    