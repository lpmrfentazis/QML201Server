from __future__ import annotations
from pydantic import BaseModel, AwareDatetime
from typing import Annotated
from annotated_types import Le, Lt, Ge



class LoggerDataModel(BaseModel): # type: ignore
    timemoment: AwareDatetime
    temperature: Annotated[float, Ge(-80), Le(60)]
    humidity: Annotated[float, Ge(0), Le(100)]
    pressure: Annotated[float, Ge(600), Le(1100)]
    windSpeed: Annotated[float, Ge(0), Le(60)]
    windDirection: Annotated[float, Ge(0), Lt(360)]
    
    def toLoggerDataModel(self) -> LoggerDataModel:
        # For child classes upcast
        return LoggerDataModel(**self.model_dump())