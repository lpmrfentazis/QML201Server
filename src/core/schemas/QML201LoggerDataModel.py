from __future__ import annotations
from .loggerDataModel import LoggerDataModel

from datetime import datetime
from typing import Union
import pytz


    
class QML201LoggerDataModel(LoggerDataModel):
        
    @classmethod
    def fromLoggerString(cls, string: str) -> QML201LoggerDataModel:
        # example: 0R0,WS=0.1C,WD=211C,TA=20.1C,RH=32C,PA=748.6C
        """
            WS - windSpeed
            WD - windDirection
            TA - temperature
            RH - humidity
            PA - pressure
        """
        params: dict[str, Union[datetime, float]] = {"timemoment": datetime.now(pytz.utc)}
        
        for param in string.strip().split(","):
            if "=" not in param:
                continue
            
            parts = param.split("=")
            if len(parts) > 1 and "C" in parts[1]:
                if parts[0] == "WS":
                    params["windSpeed"] = float(parts[1].split("C")[0])
                
                elif parts[0] == "WD":
                    params["windDirection"] = float(parts[1].split("C")[0])
                
                elif parts[0] == "TA":
                    params["temperature"] = float(parts[1].split("C")[0])
                
                elif parts[0] == "RH":
                    params["humidity"] = float(parts[1].split("C")[0])
                
                elif parts[0] == "PA":
                    params["pressure"] = float(parts[1].split("C")[0])
        
        return cls(**params)
    