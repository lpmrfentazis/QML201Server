from verboselogs import VerboseLogger
from logging.handlers import MemoryHandler
from datetime import datetime
from pathlib import Path

from time import gmtime
from datetime import datetime
import pytz

import logging
import coloredlogs



# TODO https://stackoverflow.com/questions/7385037/how-do-i-get-the-name-of-the-class-containing-a-logging-call-in-python

class Logger(VerboseLogger): # type: ignore
    def __init__(self, name: str, level: int = logging.INFO, *, path: Path | str = Path(), messageBufferSize: int = 200):
        super(Logger, self).__init__(name, level)
        
        self.level = level
        
        self.path = Path(path)
        self.path.mkdir(parents=True, exist_ok=True)
        
        fileName = datetime.now(tz=pytz.utc).strftime(f"{name}_%d-%m-%Y") + ".log"
        self.logFile = self.path / fileName

        self.messageBufferSize = messageBufferSize
        self.memory = MemoryHandler(messageBufferSize, flushLevel=999, target=None)
        
        self.file = logging.FileHandler(str(self.logFile))
        self.fileformat = logging.Formatter(
            "[~] %(asctime)s [%(levelname)s] - %(message)s")

        self.file.setLevel(self.level)
        self.file.setFormatter(self.fileformat)
        
        super().addHandler(self.file)
        super().addHandler(self.memory)

        coloredlogs.install(level=level, logger=super(),
                            fmt='%(asctime)s [%(levelname)s] - %(message)s')
        
        logging.Formatter.converter = gmtime

        super().info('Start Logger')
        
    def getBuffer(self) -> list[logging.LogRecord]:
        return self.memory.buffer
