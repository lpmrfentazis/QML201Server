from time import sleep
from threading import Thread, Event

from core.db.models.loggerDataModel import LoggerDataModel
from typing import Optional, Any
from abc import ABC, abstractmethod
from verboselogs import VerboseLogger

from datetime import datetime



class BasePnPConnection(ABC):
    _connected: bool
    _lastData: Optional[tuple[datetime, LoggerDataModel]]
    _timeout: float
    _mainThread: Optional[Thread]
    _stopMainThreadEvent: Event
    logger: VerboseLogger
    
    def __init__(self, *args: list[Any], **kwargs: dict[Any, Any]) -> None:
        raise NotImplementedError(f"An attempt to call a virtual method. It looks like you're trying to use {type(self).__name__},"\
            f"Try to inherit from {type(self).__name__} and implement the _tick, __init__, connect, disconnect methods")
    
    @abstractmethod       
    def connect(self, silent: bool = False) -> bool:
        raise NotImplementedError(f"An attempt to call a virtual method. It looks like you're trying to use {type(self).__name__},"\
            f"Try to inherit from {type(self).__name__} and implement the _tick, __init__, connect, disconnect methods")
    
    @abstractmethod
    def disconnect(self) -> bool:
        raise NotImplementedError(f"An attempt to call a virtual method. It looks like you're trying to use {type(self).__name__},"\
            f"Try to inherit from {type(self).__name__} and implement the _tick, __init__, connect, disconnect methods")
    
    def isConnected(self) -> bool:
        return self._connected
    
    def getLastData(self) -> Optional[tuple[datetime, LoggerDataModel]]:
        return self._lastData
    
    @abstractmethod
    def _tick(self) -> None:
        raise NotImplementedError(f"An attempt to call a virtual method. It looks like you're trying to use {type(self).__name__},"\
            f"Try to inherit from {type(self).__name__} and implement the _tick, __init__, connect, disconnect methods")
        
    def _mainThreadLoop(self) -> None:
        while not self._stopMainThreadEvent.is_set():
            try:
                if not self._connected:
                    if not self.connect(silent=True):
                        sleep(self._timeout)
                        continue
                
                self._tick()
                
                sleep(0.01)
            except KeyboardInterrupt:
                self.stop()
                break
    
    def start(self) -> bool:
        if self._connected or (self._mainThread and self._mainThread.is_alive()):
            return False
        
        self._stopMainThreadEvent.clear()
        self._mainThread = Thread(target = self._mainThreadLoop, name="mainThreadLoop")
        self._mainThread.start()

        self.logger.debug(f"[{type(self).__name__}]: start connection watching")
        
        return True
    
    def stop(self) -> bool:
        if not self._connected or not (self._mainThread and self._mainThread.is_alive()):
            return False
        
        self._stopMainThreadEvent.set()
        self.disconnect()
        return True
    
    def __del__(self) -> None:
        self.stop()