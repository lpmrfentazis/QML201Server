from .serialConnection import SerialConnection
from serial import SerialException

from core.db.models import QML201LoggerDataModel
from pydantic import ValidationError

from datetime import datetime
import pytz



class QML201SerialConnection(SerialConnection): # type: ignore
    _connected: bool # for fix mypy has-type err =(
    def _tick(self) -> None:
        try:
            if self._connected and self._serial:
                if self._serial.in_waiting:
                    mes = self._serial.readline()
                    mes = mes.decode('UTF-8').strip()
                    self.logger.spam(f'[{type(self).__name__}]: readed message "{mes}"')
                    
                    try:
                        self._lastData = (datetime.now(tz=pytz.utc), QML201LoggerDataModel.fromLoggerString(mes).toLoggerDataModel())
                    except ValidationError:
                        pass
                else:
                    mes = f"getlog"
                    self.logger.spam(f'[{type(self).__name__}]: sended message "{mes}"')
                    
                    self._serial.write(mes.encode("UTF-8"))
                    
        except SerialException:
            self.logger.warning(f"[{type(self).__name__}]: Disconnected {self._port}")
            self._connected = False
        
        except KeyboardInterrupt as e:
            raise e
        
        except Exception as e:
            self.logger.warning(f"[{type(self).__name__}]: Unknown error in {type(self).__name__}._tick: {e}")
            
        