from .basePnPConnection import BasePnPConnection
from .QML201SerialConnection import QML201SerialConnection

from verboselogs import VerboseLogger

from core.config.models.configLoggerModel import (ConfigLoggerSerialConnectionModel,
                                                  ConfigConnectionType,
                                                  ConfigLoggerModel,
                                                  ConfigPlatformType
                                                  )

from typing import Type

avaliablePlatformConnections: dict[tuple[ConfigPlatformType, ConfigConnectionType], Type[BasePnPConnection]] = {
    (ConfigPlatformType.QML201, ConfigConnectionType.serial): QML201SerialConnection
}


def getConnection(config: ConfigLoggerModel, logger: VerboseLogger) -> BasePnPConnection:
    if (config.platform, config.connection.type) not in avaliablePlatformConnections:
        raise ValueError(f"Unsupported pair {config.platform} and {config.connection.type}")
    
    return avaliablePlatformConnections[(config.platform, config.connection.type)](logger=logger,
                                                                                   config=config.connection)