from time import sleep
import serial
from serial.tools import list_ports
from serial.serialutil import SerialException
from threading import Thread, Event

from verboselogs import VerboseLogger

from core.db.models.loggerDataModel import LoggerDataModel
from core.config.models import ConfigLoggerSerialConnectionModel

from .basePnPConnection import BasePnPConnection
from typing import Optional

from abc import abstractmethod 
from datetime import datetime



class SerialConnection(BasePnPConnection):
    def __init__(self, 
                 config: ConfigLoggerSerialConnectionModel,
                 logger : VerboseLogger
                 ):
        self.logger: VerboseLogger = logger
        self._connected: bool = False
        self._timeout: float = config.timeout
        self.port: str = config.port
        self.baudrate: int = config.baudrate
        self.deviceVID: Optional[int] = config.deviceVID
        self._description: Optional[int] = config.description
        
        self._lastData: Optional[tuple[datetime, LoggerDataModel]] = None
        
        self._serial = None
        
        self._mainThread: Optional[Thread] = None
        self._stopMainThreadEvent: Event = Event()
        
        self.logger.info(f"[{type(self).__name__}]: init serial Connection class")
            
    def connect(self, silent: bool = False) -> bool:
        try:
            self._connected = False
            
            if self.port == "auto":
                for unit in list_ports.comports():
                    if not (self.deviceVID is None) and (unit.vid == self.deviceVID):
                        self._port = unit.device # get port 
                        
                        self.logger.info(f"[{type(self).__name__}]: The VID: {self.deviceVID} device is detected automatically on {self._port}")
                        
                        break
                    
                    elif not (self._description is None) and (self._description in unit.description):
                        # TODO Add some check
                        self._port = unit.device # get port 
                        
                        self.logger.info(f"[{type(self).__name__}]: The {self._description} device is detected automatically on {self._port}")
                            
                        break
                    
                else:
                    if not silent:
                        device = f"VID: {self.deviceVID}{f'/{self._description}' if not (self._description is None) else ''}"
                        self.logger.critical(f"[{type(self).__name__}]: The {device} device could not be detected. Perhaps the controller is not connected")
                        return self._connected
            else:
                self._port = self.port
                
            self._serial = serial.Serial(port = self._port, baudrate = self.baudrate, timeout = self._timeout)
            
            self.logger.info(f"[{type(self).__name__}]: Connection with {self._port} has been successfully with {self.baudrate} b/s")
                
            self._connected = True

        except SerialException:
            if not silent:
                self.logger.error(f"[{type(self).__name__}]: Failed connection with {self._port}")
                self._connected = False

        except Exception as e:
            if not silent:
                self.logger.critical(f"[{type(self).__name__}]: Unexpected exception with Connection.connect(): {e}")
                self._connected = False
        
        return self._connected
    
    def disconnect(self) -> bool:
        self._connected = False
        
        if self._serial:
            self._serial.close()
            self.logger.info(f"[{type(self).__name__}]: device manualy disconnected {self._port}")
            return True
        
        return False
    
    @abstractmethod
    def _tick(self) -> None:
        raise NotImplementedError(f"An attempt to call a virtual method. It looks like you're trying to use {type(self).__name__},"\
            "Try to inherit from {type(self).__name__} and implement the _tick method")
        
    def _mainThreadLoop(self) -> None:
        silent = False
        while not self._stopMainThreadEvent.is_set():
            try:
                if not self._connected:
                    if not self.connect(silent=silent):
                        sleep(self._timeout)
                        silent = False
                        continue
                
                self._tick()
                silent = True
                sleep(0.1)
            except SerialException as e:
                self.disconnect()
    
    def start(self) -> bool:
        if self._connected or (self._mainThread and self._mainThread.is_alive()):
            return False
        
        self._stopMainThreadEvent.clear()
        self._mainThread = Thread(target = self._mainThreadLoop, name="mainThreadLoop")
        self._mainThread.start()
        
        return True
    
    def stop(self) -> bool:
        if not (self._mainThread and self._mainThread.is_alive()):
            return False
        
        self._stopMainThreadEvent.set()
        return True
                