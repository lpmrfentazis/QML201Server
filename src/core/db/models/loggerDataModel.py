from __future__ import annotations
from core.schemas.loggerDataModel import LoggerDataModel

from .baseDB import BaseDB
from sqlalchemy.orm import mapped_column, Mapped
from datetime import datetime
import pytz


class LoggerDataDBModel(BaseDB):
    __tablename__ = "logger_data"

    id: Mapped[int] = mapped_column(primary_key=True)
    timemoment: Mapped[datetime] = mapped_column(default= lambda: datetime.now(pytz.utc), nullable=False)
    temperature: Mapped[float] = mapped_column(nullable=False)
    humidity: Mapped[float] = mapped_column(nullable=False)
    pressure: Mapped[float] = mapped_column(nullable=False)
    windSpeed: Mapped[float] = mapped_column(nullable=False)
    windDirection: Mapped[float] = mapped_column(nullable=False)
    
    def toPydanticModel(self) -> LoggerDataModel:
        return LoggerDataModel(timemoment=pytz.utc.localize(self.timemoment), 
                               temperature=self.temperature,
                               humidity=self.humidity,
                               pressure=self.pressure,
                               windSpeed=self.windSpeed,
                               windDirection=self.windDirection)
    @classmethod
    def fromPydanticModel(cls, model: LoggerDataModel) -> LoggerDataDBModel:
        return LoggerDataDBModel(timemoment=model.timemoment,
                            temperature=model.temperature,
                            humidity=model.humidity,
                            pressure=model.pressure,
                            windSpeed=model.windSpeed,
                            windDirection=model.windDirection)
