from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.future import select
from sqlalchemy import desc

from pathlib import Path
from verboselogs import VerboseLogger

from .models import BaseDB, LoggerDataDBModel
from core.schemas.loggerDataModel import LoggerDataModel
from typing import Annotated
from annotated_types import Ge, Le

from datetime import datetime
import asyncio


class Database:
    def __init__(self, dsn: str, logger: VerboseLogger):

        self.logger = logger

        self.logger.info(f"[{type(self).__name__}]: Init database")

        self.db = create_async_engine(dsn, echo=False)
        self.connected = False
    
    async def _connect(self) -> bool:
        try:
            async with self.db.begin() as conn:
                await conn.run_sync(BaseDB.metadata.create_all)
            
            self.connected = True
            self.logger.info(f"[{type(self).__name__}]: database connected")
            
        except:
            self.connected = False
            self.logger.critical(f"[{type(self).__name__}]: database connection failed")
            
        return self.connected
    
    def connect(self) -> bool:
        return asyncio.run(self._connect())
    
    def disconnect(self) -> bool:
        self.connected = False
        return not self.connected
    
    def __validateLimitOffset(self, offset: Annotated[int, Ge(0)], limit: Annotated[int, Ge(0), Le(1000)]) -> bool:
        if limit > 1000:
            limit = 1000
            
            self.logger.warning(f"[{type(self).__name__}]: getLoggerDataX limit mast been <= 1000, but given {limit}")
        
        elif limit < 0:
            self.logger.warning(f"[{type(self).__name__}]: getLoggerDataX limit mast been > 0, but given {limit}")
            return False
        
        if offset < 0:
            self.logger.warning(f"[{type(self).__name__}]: getLoggerDataX offset mast been > 0, but given {limit}")
            offset = 0
        
        return True  
    
    async def addLoggerData(self, loggerData: LoggerDataModel) -> None:
        async with AsyncSession(self.db) as session:
            async with session.begin():
                session.add(LoggerDataDBModel.fromPydanticModel(loggerData))
                self.logger.debug(f"[{type(self).__name__}]: Write loggerData to db: {loggerData}")
    
    # TODO FIX ME ?    
    def addLoggerDataSync(self, loggerData: LoggerDataModel) -> None:
        return asyncio.run(self.addLoggerData(loggerData=loggerData))
                
    async def getLastLoggerData(self) -> LoggerDataModel:
        async with AsyncSession(self.db) as session:
            async with session.begin():
                loggerDataDB = await session.scalar(select(LoggerDataDBModel).order_by(desc(LoggerDataDBModel.id)).limit(1))
                
                return loggerDataDB.toPydanticModel()
    
    def getLastLoggerDataSync(self) -> LoggerDataModel:
        return asyncio.run(self.getLastLoggerData())
       
    async def getLoggerData(self, 
                            offset: Annotated[int, Ge(0)] = 0,
                            limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
        
        if not self.__validateLimitOffset(limit, offset):
            return []
        
        async with AsyncSession(self.db) as session:
            res = await session.scalars(select(LoggerDataDBModel)
                                        .order_by(desc(LoggerDataDBModel.id))
                                        .limit(limit)
                                        .offset(offset))
            
            return [i.toPydanticModel() for i in res.all()]
    
    def getLoggerDataSync(self, offset: Annotated[int, Ge(0)] = 0, 
                            limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
        
        return asyncio.run(self.getLoggerData(limit=limit, offset=offset))
           
    async def getLoggerDataYoungerThan (self, timemoment: datetime, 
                                        offset: Annotated[int, Ge(0)] = 0, 
                                        limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
        
        if not self.__validateLimitOffset(limit, offset):
            return []
        
        async with AsyncSession(self.db) as session:
            res = await session.scalars(select(LoggerDataDBModel)
                                        .filter(LoggerDataDBModel.timemoment >= timemoment)
                                        .limit(limit).offset(offset))
            
            return [i.toPydanticModel() for i in res.all()]
    
    def getLoggerDataYoungerThanSync(self, timemoment: datetime, 
                                        offset: Annotated[int, Ge(0)] = 0, 
                                        limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
        return asyncio.run(self.getLoggerDataYoungerThan(timemoment=timemoment, limit=limit, offset=offset))
    
    async def getLoggerDataOlderThan (self, 
                                        timemoment: datetime,
                                        offset: Annotated[int, Ge(0)] = 0, 
                                        limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
        
        if not self.__validateLimitOffset(limit, offset):
            return []
        
        async with AsyncSession(self.db) as session:
            res = await session.scalars(select(LoggerDataDBModel)
                                        .filter(LoggerDataDBModel.timemoment <= timemoment)
                                        .limit(limit).offset(offset))
            
            return [i.toPydanticModel() for i in res.all()]
    
    def getLoggerDataOlderThanSync(self, 
                                        timemoment: datetime,  
                                        offset: Annotated[int, Ge(0)] = 0,
                                        limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
        
        return asyncio.run(self.getLoggerDataOlderThan(timemoment=timemoment,
                                                       limit=limit,
                                                       offset=offset))
        
    async def getLoggerDataInRange(self, timeStart: datetime, 
                                        timeEnd: datetime, 
                                        offset: Annotated[int, Ge(0)] = 0,
                                        limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
        
        if not self.__validateLimitOffset(limit=limit, offset=offset):
            return []
        
        if not (timeStart < timeEnd):
            self.logger.warning(f"[{type(self).__name__}]: getLoggerDataInRange timeStart mast been < timeEnd, but given {timeStart} and {timeEnd}")
            return []
        
        async with AsyncSession(self.db) as session:
            res = await session.scalars(select(LoggerDataDBModel)
                                        .filter(LoggerDataDBModel.timemoment >= timeStart)
                                        .filter(LoggerDataDBModel.timemoment <= timeEnd)
                                        .order_by(desc(LoggerDataDBModel.id))
                                        .limit(limit)
                                        .offset(offset))
            
            return [i.toPydanticModel() for i in res.all()]
        
    def getLoggerDataInRangeSync(self, timeStart: datetime, 
                                        timeEnd: datetime, 
                                        offset: Annotated[int, Ge(0)] = 0, 
                                        limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
        
        return asyncio.run(self.getLoggerDataInRange(timeStart=timeStart, 
                                                         timeEnd=timeEnd, 
                                                         limit=limit, 
                                                         offset=offset))
        
    def __del__(self) -> None:
        self.disconnect()