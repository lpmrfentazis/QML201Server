from __future__ import annotations
from core.shared.threadSafeSingleton import ThreadSafeSingleton
from core.config.config import ConfigMainModel
from core.connection.basePnPConnection import BasePnPConnection
from core.connection import getConnection
from core.db.database import Database
from verboselogs import VerboseLogger

from threading import Thread, Event
from time import sleep
from datetime import datetime
import pytz



class Master(metaclass=ThreadSafeSingleton): # type: ignore[misc]  
    """
        It's is Singleton
    """
    def __init__(self, config: ConfigMainModel, logger: VerboseLogger):
        self.config = config
        self.logger = logger
        
        self.__mainThread: Thread = Thread(name="MasterMainLoop", target=self.__mainThreadLoop)
        self.__stopMainThreadEvent: Event = Event()
        
        self.__lastDataUpdate: datetime = datetime.fromtimestamp(0, tz=pytz.utc)
    
        self.db = Database(dsn=self.config.database.dsn, 
                           logger=self.logger)
        self.connection: BasePnPConnection = getConnection(config=self.config.logger, logger=self.logger)
    
    def __mainThreadLoop(self) -> None:
        while not self.__stopMainThreadEvent.is_set():
            try:
                try:
                    lastData = self.connection.getLastData()
                    if lastData:
                        timemoment, data = lastData
                        if timemoment > self.__lastDataUpdate:
                            self.__lastDataUpdate = datetime.now(pytz.utc)
                            self.db.addLoggerDataSync(data)
                        
                    sleep(self.config.logger.getInfoTimeout)
                
                except KeyboardInterrupt:
                    self.stop()
            
            except Exception as e:
                self.logger.error(f"[{type(self).__name__}]: Unknown exception {e}")
    
    def stop(self) -> bool:
        self.__stopMainThreadEvent.set()
        self.connection.stop()
        self.db.disconnect()
        self.connection.stop()
        return True
    
    def start(self) -> bool:        
        if self.__mainThread.is_alive():
            self.logger.warning(f"[{type(self).__name__}]: Attempt to start Master.start() while the main thread is still active")
            return False
        
        self.__stopMainThreadEvent.clear()
        
        if not self.db.connect():
            return False
        
        if not self.connection.start():
            return False
        
        self.logger.debug(f"[{type(self).__name__}]: start master watching")
        self.__mainThread.start()
        
        return True
    
    def __enter__(self) -> Master:
        self.start()
        
        return self
    
    def __exit__(self, exceptionType: type, exceptionValue: str, exceptionVraceback: str) -> None:
        #Exception handling here
        self.stop()
    
    def __del__(self) -> None:
        self.stop()


def getMasterInstance() -> Master:
    try:
        return Master() # type: ignore[call-arg]
    
    except TypeError:
        raise NotImplementedError("Attempt to get instance of Master before initialization")