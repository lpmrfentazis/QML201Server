from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from core.config.config import ConfigMainModel

from .api import masterRouter

def getApplication(config: ConfigMainModel) -> FastAPI:
    # do some settings
    app = FastAPI()
    
    ## Allow CORS
    if config.webServer.allowCORS:
        app.add_middleware(
            CORSMiddleware,
            allow_origins=["*"],
            # allow_origins=ALLOWED_HOSTS or ["*"],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )
    
    app.include_router(masterRouter)
    
    return app