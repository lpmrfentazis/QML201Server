from fastapi import APIRouter
from fastapi.responses import RedirectResponse

swaggerRouter = APIRouter()

@swaggerRouter.get("/") # type: ignore[misc]
async def toSwagger() -> RedirectResponse:
    """
        Redirect to SwaggerUI page no frontend yet
        Replace me if it shows up
    """
    return RedirectResponse("/docs")