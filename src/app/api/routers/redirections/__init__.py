from fastapi import APIRouter
from .swagger import swaggerRouter

redirectionsRouter = APIRouter()
redirectionsRouter.include_router(swaggerRouter)