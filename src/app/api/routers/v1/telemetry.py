from fastapi.responses import RedirectResponse
from fastapi import APIRouter, status, HTTPException

from typing import Annotated, Optional
from annotated_types import Ge, Le
from pydantic import AwareDatetime

from core.db.models.loggerDataModel import LoggerDataModel
from core.master import getMasterInstance



telemetry = APIRouter(prefix="/telemetry", tags=["telemetry"])

@telemetry.get("/records/last") # type: ignore[misc]
async def getLastRecords() -> LoggerDataModel:
    return await getMasterInstance().db.getLastLoggerData()

@telemetry.get("/records") # type: ignore[misc]
async def getRecords(offset: Annotated[int, Ge(0)] = 0,
                        limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
    return await getMasterInstance().db.getLoggerData(offset=offset, limit=limit) # type: ignore[no-any-return]

@telemetry.get("/records/range") # type: ignore[misc]
async def getRecordsRange(yanger: Optional[AwareDatetime] = None, # type: ignore[return]
                        older: Optional[AwareDatetime] = None,
                        offset: Annotated[int, Ge(0)] = 0,
                        limit: Annotated[int, Ge(0), Le(1000)] = 10) -> list[LoggerDataModel]:
    
    if not yanger and not older:
        return RedirectResponse(f"/records?offset={offset}&limit={limit}") # type: ignore[no-any-return]
    
    if yanger and not older:
        return await getMasterInstance().db.getLoggerDataYoungerThan(timemoment=yanger, limit=limit, offset=offset) # type: ignore[no-any-return]
    
    elif not yanger and older:
        return await getMasterInstance().db.getLoggerDataOlderThan(timemoment=yanger, limit=limit, offset=offset) # type: ignore[no-any-return]
    
    elif yanger and older:
        if not(yanger >= older):
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail="Yanger mast be >= older")
        
        return await getMasterInstance().db.getLoggerDataInRange(timeStart=older, timeEnd=yanger, limit=limit, offset=offset) # type: ignore[no-any-return]