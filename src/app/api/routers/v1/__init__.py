from fastapi import APIRouter
from .telemetry import telemetry
from .pages import pages

v1Router = APIRouter(prefix="/v1")
v1Router.include_router(telemetry)
v1Router.include_router(pages)