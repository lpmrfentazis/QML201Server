from fastapi import APIRouter

from .routers.v1 import v1Router
from .routers.redirections import redirectionsRouter


masterRouter = APIRouter()

masterRouter.include_router(v1Router)

# mast be last
masterRouter.include_router(redirectionsRouter)