src package
===========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   src.core
   src.tests
   src.v1

Submodules
----------

src.main module
---------------

.. automodule:: src.main
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src
   :members:
   :undoc-members:
   :show-inheritance:
