# Determine the path of the current directory
$CURRENT_DIR = Get-Location
$CURRENT_DIR = "$CURRENT_DIR\src"

# Check if main.py and requirements.txt exist in the current directory
if ((Test-Path "$CURRENT_DIR\main.py") -and (Test-Path "$CURRENT_DIR\..\requirements.txt")) {
    $APPPATH = $CURRENT_DIR
} else {
    # Check parent directories until finding main.py and requirements.txt
    $PARENT_DIR = $CURRENT_DIR
    while ($PARENT_DIR -ne "/") {
        $PARENT_DIR = Split-Path $PARENT_DIR -Parent
        if ((Test-Path "$PARENT_DIR\main.py") -and (Test-Path "$PARENT_DIR\..\requirements.txt")) {
            $APPPATH = $PARENT_DIR
            break
        }
    }

    if (-not $APPPATH) {
        Write-Host "Error: Could not find main.py and requirements.txt in current or parent directories."
        exit 1
    }
}

Write-Host "Application path: $APPPATH"

# Path to the venv folder
$VENVPATH = Join-Path $APPPATH "venv"

# Check if the venv folder exists
if (-not (Test-Path $VENVPATH -PathType Container)) {
    Write-Host "Creating virtual environment..."
    python -m venv $VENVPATH
}

Write-Host "Init git submodule's"
git submodule update --init --recursive

# Activate the virtual environment
$activate_script = Join-Path $VENVPATH "Scripts\Activate.ps1"
. $activate_script

# Install dependencies from requirements.txt
Write-Host "Installing dependencies..."
pip install -r "$APPPATH\..\requirements.txt"

Write-Host "Installing submodules dependencies..."
git submodule --quiet foreach --recursive pip install -r requirements.txt

# Run main.py
Write-Host "Running main.py..."
python "$APPPATH\main.py"

# Deactivate the virtual environment
deactivate
