@echo off

echo Determine the path of the current directory
rem Determine the path of the current directory
set "CURRENT_DIR=%CD%\src"

echo %CURRENT_DIR%

echo Check if main.py and requirements.txt exist in the current directory
rem Check if main.py and requirements.txt exist in the current directory
if exist "%CURRENT_DIR%\main.py" (
    if exist "%CURRENT_DIR%\..\requirements.txt" (
        set "APPPATH=%CURRENT_DIR%"
    )
)

echo Check parent directories until finding main.py and requirements.txt
rem Check parent directories until finding main.py and requirements.txt
if not defined APPPATH (
    set "PARENT_DIR=%CD%"
    :LOOP
    for %%I in ("%PARENT_DIR%") do set "PARENT_DIR=%%~dpI"
    if exist "%PARENT_DIR%\main.py" (
        if exist "%PARENT_DIR%\..\requirements.txt" (
            set "APPPATH=%PARENT_DIR%"
        )
    )
    if "%PARENT_DIR%" neq "\"  (
        if "%PARENT_DIR%" neq "" (goto :LOOP)
    )
)

if not defined APPPATH (
    echo Error: Could not find main.py and requirements.txt in current or parent directories.
    exit /b 1
)

echo Application path: %APPPATH%

rem Path to the venv folder
set "VENVPATH=%APPPATH%\venv"

rem Check if the venv folder exists
if not exist "%VENVPATH%" (
    echo Creating virtual environment...
    python -m venv "%VENVPATH%"
)

rem Init git submodule's
echo "Init git submodule's"
call git submodule update --init --recursive

rem Activate the virtual environment
call "%VENVPATH%\Scripts\activate"

rem Install dependencies from requirements.txt
echo Installing dependencies...
pip install -r "%APPPATH%\..\requirements.txt"

rem Install dependencies from submodeles requirements.txt
echo Installing submodules dependencies...
git submodule --quiet foreach --recursive pip install -r requirements.txt

rem Run main.py
echo Running main.py...
python "%APPPATH%\main.py"

rem Deactivate the virtual environment
deactivate
